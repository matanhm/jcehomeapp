import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Posts } from './interfaces/posts';
import { Users } from './interfaces/users';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  apiUrlposts = 'https://jsonplaceholder.typicode.com/posts/';
  apiUrlusers = 'https://jsonplaceholder.typicode.com/users/';
  userCollection: AngularFirestoreCollection = this.db.collection('users');
  postCollection: AngularFirestoreCollection;


  constructor(private _http: HttpClient, private db:AngularFirestore) { }
  
  getPosts(userId:string): Observable<any[]>{
   // return this.db.collection('posts').valueChanges({idField:'id'});
   this.postCollection = this.db.collection(`users/${userId}/posts`);
   return this.postCollection.snapshotChanges().pipe(
     map(
       collection => collection.map(
         document => {
           const data = document.payload.doc.data();
           data.id = document.payload.doc.id;
           return data;
         }
       )
 
     )
   )
 
  }

  //getPosts(){
    //return this._http.get<Posts[]>(this.apiUrlposts);
  //}
  //getUsers(){
    //return this._http.get<Users[]>(this.apiUrlusers);
  //}

  addPost(userId:string,title:string, body:string, author:string){
    const post = {title:title,body:body,author:author}
    //this.db.collection('posts').add(post)  
    this.userCollection.doc(userId).collection('posts').add(post);
  }  
  updatePost(userId:string, id:string, title:string, body:string, author:string){
    this.db.doc(`users/${userId}/posts/${id}`).update({
      title:title,
      body:body,
      author:author
    })
    
  }
  
  getPost(id:string, userId:string): Observable<any>{
    return this.db.doc(`users/${userId}/posts/${id}`).get()
  }

  deletePost(id:string, userId:string){
    this.db.doc(`users/${userId}/posts/${id}`).delete();
  }

}
