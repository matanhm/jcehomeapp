import { AuthorsService } from './../authors.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
@Component({
 selector: 'app-authors',
 templateUrl: './authors.component.html',
 styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {
  panelOpenState = false;
  authors:any;
  authors$:Observable<any>;
  name;
 constructor(private authorsservice:AuthorsService) { }


 addAuthors(name){
  // this.authors$ + name;
   this.authorsservice.addAuthors(name);
    }
  
  ngOnInit() {
    //this.authors = this.authorsservice.getAuthors().subscribe(
     // (authors) => this.authors = authors
   // )
   this.authors$ = this.authorsservice.getAuthors();
  }

}