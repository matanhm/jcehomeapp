import { AuthService } from './../auth.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { Users } from './../interfaces/users';
import { PostsService } from './../posts.service';
import { Component, OnInit } from '@angular/core';
import { Posts } from '../interfaces/posts';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  postsData$: Observable<any[]>;
 // users$: Users[];
  //postsdb$: Observable<any>;
  userId:string;
  constructor(private PostsService: PostsService, public AuthService:AuthService) { }

  ngOnInit() {
   /// this.postsData$ = this.PostsService.getPosts();
   this.AuthService.user.subscribe(
    user => {
      this.userId = user.uid;
      this.postsData$ = this.PostsService.getPosts(this.userId);
    }
  )
 }

    
  
  deletePost(id:string){
    this.PostsService.deletePost(id, this.userId);
  }
 
}
