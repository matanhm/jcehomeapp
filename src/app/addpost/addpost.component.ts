import { AuthService } from './../auth.service';
import { AuthorsService } from './../authors.service';
import { Router, ActivatedRoute } from '@angular/router';
import { PostsService } from './../posts.service';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-addpost',
  templateUrl: './addpost.component.html',
  styleUrls: ['./addpost.component.css']
})
export class AddpostComponent implements OnInit {

  constructor(private postsservice:PostsService, private router:Router,
    private route:ActivatedRoute, private authorservice:AuthorsService,
    private authservice:AuthService) { }

  id:string;
  title:string;
  body:string;
  author:string;
  userId:string;

  isEdit:boolean = false;
  buttonText:string = "Add Post" ;

  onSubmit(){
    if(this.isEdit){
      this.postsservice.updatePost(this.userId,this.id,this.title,this.body,this.author)
    }
    else{
      this.postsservice.addPost(this.userId,this.title,this.body,this.author)
    }
      this.router.navigate(['/posts']);
  }

  ngOnInit() {
    this.id = this.route.snapshot.params.id; 
    this.authservice.user.subscribe(
      user=> {
        this.userId = user.uid;
        if(this.id){
          this.isEdit =true;
          this.buttonText ="Update Post";
          this.postsservice.getPost(this.id,this.userId).subscribe(
            post => {
              this.title = post.data().title;
              this.body = post.data().body;
              this.author = post.data().author;
            })
        }
      }
    )
  }

}
