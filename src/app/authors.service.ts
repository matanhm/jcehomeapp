import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthorsService {

  authors: any = ['Lewis Carrol', 'Leo Tolstoy', 'Thomas Mann'];
  newauthor: any;
  getAuthors(){ 
    const authorsObservable = new Observable(
      observer => {
        setInterval(
          () => observer.next(this.authors),4000)
      }
    )
    return authorsObservable;
  }
  addAuthors = (name) => this.authors.push(name);


 
 
  constructor() { }
}
