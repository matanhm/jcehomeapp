// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebaseConfig : {
    apiKey: "AIzaSyDi-k69kNxnOkbRD7iDRB6_Msgdes-CNFE",
    authDomain: "jcehomeapp.firebaseapp.com",
    databaseURL: "https://jcehomeapp.firebaseio.com",
    projectId: "jcehomeapp",
    storageBucket: "jcehomeapp.appspot.com",
    messagingSenderId: "28858149776",
    appId: "1:28858149776:web:1e3fc28d687f52beb5a115"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
